import VueStackedCarousel from './VueStackedCarousel.vue'
import VueStackedCarouselExtended from './VueStackedCarouselExtended.vue'

export { 
    VueStackedCarousel,
    VueStackedCarouselExtended
}
